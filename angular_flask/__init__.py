# Initial imports for angular_flask.
import os
import json
from flask import Flask, request, Response
from flask import render_template, send_from_directory, url_for, redirect, session, abort

# Flask Application.
app = Flask(__name__)
app.config.from_object('angular_flask.settings')
app.url_map.strict_slashes = False

# Controllers
import angular_flask.controllers
