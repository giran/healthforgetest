# Imports.
import os
import requests

from flask import Flask, request, Response, jsonify 
from flask import render_template, url_for, redirect, send_from_directory
from flask import send_file, make_response, abort, session

from angular_flask import app


# routing for basic pages (pass routing onto the Angular app)
@app.route('/')
def basic_pages(**kwargs):
    return make_response(open('angular_flask/templates/index.html').read())

# Route into external API.
@app.route('/getPatients')
def get_patients():

    # Make external request to HealthForge API.
    url = "https://api.interview.healthforge.io:443/api/patient"
    params = {"size": 500}
    headers = {"api_key": "swagger"}
    req = requests.get(url=url, headers=headers, params=params)

    ## Use jsonify to encode data.
    return jsonify(req.json())

# Route into patient page.
@app.route('/displayPatient')
def display_patient():
    return render_template('userData.html')


# special file handlers and error handlers
@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'img/favicon.ico')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
