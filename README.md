# HealthForge API Test - Unsecure Portal

This repo serves as the submission to the first requirement of the technical test.
Upon initialisation of the web server, the main page will display the list of patients powered by ngTable.
Clicking on an individual patient will display another window with the appropriate details of the patient from the API response.

### How to Get Started:

1. Clone this repository to the desired location.

2. Install all the necessary packages - ideally inside of a virtual environment.
> pip install -r requirements.txt

3. Run 'runserver.py' to start the web application.
> python runserver.py

4. View the patient data through the main page:
> http://localhost:4444/

#### Notes:
- The basis for this web app was provided by https://github.com/shea256/angular-flask but has been extensively modified for the requirements of the test.
- For efficiency, we have restricted the downloading of patient data to 500 records. This can be changed in controllers.py
- The backend is powered by Flask while the frontend is served through AngularJS and Bootstrap.